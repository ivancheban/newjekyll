I"8	<h2 id="about-syntax-highlighting">About syntax highlighting</h2>
<p>For syntax highlighting, use fenced code blocks optionally followed by the language syntax you want:</p>

<pre>
```java
import java.util.Scanner;

public class ScannerAndKeyboard
{

	public static void main(String[] args)
	{	Scanner s = new Scanner(System.in);
		System.out.print( "Enter your name: "  );
		String name = s.nextLine();
		System.out.println( "Hello " + name + "!" );
	}
}
```
</pre>

<p>This looks as follows:</p>

<pre><code class="language-java">import java.util.Scanner;

public class ScannerAndKeyboard
{

	public static void main(String[] args)
	{	Scanner s = new Scanner(System.in);
		System.out.print( "Enter your name: "  );
		String name = s.nextLine();
		System.out.println( "Hello " + name + "!" );
	}
}
</code></pre>

<p>Fenced code blocks require a blank line before and after.</p>

<p>If you’re using an HTML file, you can also use the <code>highlight</code> command with Liquid markup.</p>

<pre>
{% highlight java %}
import java.util.Scanner;

public class ScannerAndKeyboard
{

	public static void main(String[] args)
	{	Scanner s = new Scanner(System.in);
		System.out.print( "Enter your name: "  );
		String name = s.nextLine();
		System.out.println( "Hello " + name + "!" );
	}
}
{% endhighlight %}
</pre>

<p>Result:</p>

<figure class="highlight"><pre><code class="language-java" data-lang="java">import java.util.Scanner;

public class ScannerAndKeyboard
{

	public static void main(String[] args)
	{	Scanner s = new Scanner(System.in);
		System.out.print( &quot;Enter your name: &quot;  );
		String name = s.nextLine();
		System.out.println( &quot;Hello &quot; + name + &quot;!&quot; );
	}
}</code></pre></figure>

<p>The theme has syntax highlighting specified in the configuration file as follows:</p>

<pre><code>highlighter: rouge
</code></pre>

<p>The syntax highlighting is done via the css/syntax.css file.</p>

<h2 id="available-lexers">Available lexers</h2>

<p>The keywords you must add to specify the highlighting (in the previous example, <code>ruby</code>) are called “lexers.” You can search for “lexers.” Here are some common ones I use:</p>

<ul>
  <li>js</li>
  <li>html</li>
  <li>yaml</li>
  <li>css</li>
  <li>json</li>
  <li>php</li>
  <li>java</li>
  <li>cpp</li>
  <li>dotnet</li>
  <li>xml</li>
  <li>http</li>
</ul>

:ET